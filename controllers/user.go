package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/akhil/mongo-golang/models"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UserController struct {
	session *mgo.Session
}

func NewUserController(session *mgo.Session) *UserController {
	return &UserController{session}
}

func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(http.StatusNotFound)
	}

	objectId := bson.ObjectIdHex(id)
	userModel := models.User{}

	err := uc.session.DB("mongo-golang").C("users").FindId(objectId).One(&userModel)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	userJson, err := json.Marshal(userModel)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", userJson)

}

func (uc UserController) CreateUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	userModel := models.User{}

	json.NewDecoder(r.Body).Decode(&userModel)
	userModel.Id = bson.NewObjectId()

	uc.session.DB("mongo-golang").C("users").Insert(userModel)

	userJson, err := json.Marshal(userModel)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s\n", userJson)
}

func (uc UserController) DeleteUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id := p.ByName("id")

	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(404)
		return
	}

	objectId := bson.ObjectIdHex(id)

	err := uc.session.DB("mongo-golang").C("users").Remove(objectId)
	if err != nil {
		w.WriteHeader(404)
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "Deleted user", objectId, "\n")
}
